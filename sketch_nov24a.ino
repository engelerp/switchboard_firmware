/* Firmware for miniswitch prototype board
 * 
 * Author: Pascal Engeler
 * Date: 24.11.2020
 * Revision: 1.0
 * Target MCU: Arduino Due
 * IDE Version: Arduino 1.8.13
 * 
 * Omission of blocks qualified as *** CRITICAL *** will damage the hardware
 * Firmware should always be flashed with miniswitch PCB disconnected
 * Avoid holding the Arduino Due in reset while the miniswitch is connected
 * 
 * Break-before-make is ensured by the implementation
 * 
 * Interface description:
 * OPCODES:
 * - c d1 d0:
 * d1, d0 are ASCII encoded digits
 * switch all channels off, then switch channel 10*d1 + d0 on
 * - o f f
 * switch all channels off
 * 
 * The device responds via RX:
 * - a
 * ACK, the last command was valid and it was executed
 * - n
 * NACK, the last command was invalid and it was not executed
 * 
 * A response implies that handling of the command has finished
 */

/*
 * #Defines
 */
 #define BAUD_HOST 9600
 #define BAUD_DISP 9600

 #define NUM_ALL_CHANNELS 50
 #define NUM_ENABLED_CHANNELS 10


/*
 * Global variables
 */
 //display string
 char dispString[10];
 //All possible channels
 unsigned allChannels[NUM_ALL_CHANNELS] = {2,3,4,5,6,7,8,9,10,11,12,13,14,15,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53};
 //channels present on the prototype board
 unsigned enabledChannels[NUM_ENABLED_CHANNELS] = {4, 9, 10, 12, 13, 14, 15, 20, 23, 46};
 //Currently active channel, 69 implies 'straight from boot'
 unsigned currentChannel = 69;


/*
 * Functions
 */
 //Reset displayString
 void reset_displaystring(){
  sprintf(dispString,"%c%c%c%c%c%c%c%c%c%c",' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
 }
 //ACK command is valid and was executed
 void ack(){
  Serial.write('A');
 }
 //NACK command was invalid and was not executed
 void nack(){
  Serial.write('N');
 }
 //Checks if a channel is enabled
 bool check_channel(unsigned channel){
  for(unsigned i = 0; i < NUM_ENABLED_CHANNELS; ++i){
    if(enabledChannels[i] == channel){
      return true;
    }
  }
  return false;
 }
 //*** CRITICAL ***
 //Set all channels LOW
 void set_all_low(){
  for(unsigned i = 0; i < NUM_ENABLED_CHANNELS; ++i){
    digitalWrite(enabledChannels[i], LOW);
  }
 }
 //Display clearing
 void display_clear(){
  Serial2.write(0x76);
 }
 //Display switching
 void display_switch(){
  display_clear();
  sprintf(dispString, "%c%c%c%c", 'C', 'H', '-', '-');
  Serial2.write(dispString);
  reset_displaystring();
 }
 //Display channel
 void display_channel(unsigned channel){
  display_clear();
  sprintf(dispString, "%c%c%02d", 'C', 'H', channel);
  Serial2.write(dispString);
  reset_displaystring();
 }
 //Display chars
 void display_chars(char c0, char c1, char c2, char c3){
  display_clear();
  sprintf(dispString, "%c%c%c%c", c0, c1, c2, c3);
  Serial2.write(dispString);
  reset_displaystring();
 }
 //Display off
 void display_off(){
  display_clear();
  sprintf(dispString, "%c%c%c%c", ' ', 'O', 'F', 'F');
  Serial2.write(dispString);
  reset_displaystring();
 }

void setup() {

  //*** CRITICAL ***
  //channel setup, set to default state
  for(unsigned i = 0; i < NUM_ALL_CHANNELS; ++i){
    pinMode(allChannels[i], OUTPUT);
    digitalWrite(allChannels[i], LOW);
  }

  //Serial connection to host
  Serial.begin(BAUD_HOST);
  while(!Serial){
    delay(1);
  }

  //Serial connection to display
  Serial2.begin(BAUD_DISP);
  while(!Serial2){
    delay(1);
  }
  //Display setup
  Serial2.write(0x76); //clear display
  Serial2.write(0x7A); //set brightness...
  Serial2.write(255); //...to maximum
  sprintf(dispString, "%c%c%c%c", 'B', 'O', 'O', 'T');
  Serial2.write(dispString); //print boot message

  delay(1337);
  display_chars(' ', 'R', 'D', 'Y');
}

void loop() {
  if(Serial.available() >= 3){
    int b0 = Serial.read();
    int b1 = Serial.read();
    int b2 = Serial.read();
    if(b0 == (int)'c'){ //Channel switch requested
      unsigned reqChannel = (unsigned)(10*(b1 - (int)('0')) + (b2 - (int)('0')));
      if(check_channel(reqChannel)){ //Channel valid
        display_switch();
        set_all_low(); //*** CRITICAL *** set all channels LOW
        delay(333); ////*** CRITICAL *** break before make
        digitalWrite(reqChannel, HIGH); //set requested channel high
        display_channel(reqChannel);
        delay(200); //wait for make
        ack();
        currentChannel = reqChannel;
      }
      else{
        display_chars(' ','E','R','R');
        delay(200);
        display_channel(reqChannel);
        delay(200);
        if(currentChannel != 69){
          display_chars('-','-','-','-');
          delay(100);
          display_channel(currentChannel);
        }
        else{
          display_chars('-','-','-','-');
          delay(100);
          display_chars(' ','R','D','Y');
        }
        nack();
      }  
    }
    else if(b0 == (int)'o' && b1 == (int)'f' && b2 == (int)'f'){ //all channels off requested
      set_all_low();
      display_off();
      ack();
    }
    else{ //invalid command received
      display_chars('E',(char)(b0),(char)(b1),(char)(b2));
      delay(200);
      if(currentChannel != 69){
        display_chars('-','-','-','-');
        delay(100);
        display_channel(currentChannel);
      }
      else{
        display_chars('-','-','-','-');
        delay(100);
        display_chars(' ','R','D','Y');
      }
      nack();
    }
  }
}
